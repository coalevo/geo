/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.geo.service;

/**
 * This is a tagging interface for the bundle configuration.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface GeoServiceConfiguration {

  public static final String DATA_SOURCE_KEY = "datasource";
  public static final String CONNECTION_POOLSIZE_KEY = "connections.poolsize";
  public static final String DEFAULT_LANGUAGE_KEY = "default.language";
  public static final String IMPORT_DIR_KEY = "import.dir";
  
}//interface GeoServiceConfiguration
