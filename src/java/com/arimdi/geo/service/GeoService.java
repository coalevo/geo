/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.geo.service;

import com.arimdi.geo.model.GeoLocation;
import com.arimdi.geo.model.GeoLocationName;
import com.arimdi.geo.model.GeoServiceException;
import com.arimdi.geo.model.NoSuchLocationException;
import net.coalevo.foundation.model.Agent;

import java.util.Set;

/**
 * This interface defines the contract for the Geo Service that
 * provides lists and names of
 * <ol>
 * <li>Continents;</li>
 * <li>Countries;</li>
 * <li>Regions; and</li>
 * <li>Cities.</li>
 * </ol>
 * It also provides access to timezone information.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface GeoService {

  /**
   * Returns the geo identifier of a given name (may be in any language).
   *
   * @param name the name of a geo location.
   * @return the geo identifier as long.
   * @throws GeoServiceException     if an error occurs while executing this action.
   * @throws NoSuchLocationException if a location with this name does not exist.
   */
  public long getIdentifierForName(String name)
      throws GeoServiceException, NoSuchLocationException;

  /**
   * Convenience method that will retrieve the location's name
   * in the default language.
   *
   * @param gid the location's geonames identifier.
   * @return the {@link GeoLocationName} corresponding to the identifier.
   * @throws GeoServiceException     if an error occurs while executing this action.
   * @throws NoSuchLocationException if a location with this name does not exist.
   */
  public GeoLocationName getName(long gid)
      throws GeoServiceException, NoSuchLocationException;

  /**
   * Convenience method that will retrieve the location's name in a specific language.
   *
   * @param gid the location's geonames identifier.
   * @return the {@link GeoLocationName} corresponding to the identifier.
   * @throws GeoServiceException     if an error occurs while executing this action.
   * @throws NoSuchLocationException if a location with this name does not exist.
   */
  public GeoLocationName getName(long gid, String lang)
      throws GeoServiceException, NoSuchLocationException;

  /**
   * Lists the location's names in different languages.
   *
   * @param gid the location's geonames identifier.
   * @return a set of {@link GeoLocationName}.
   * @throws GeoServiceException     if an error occurs while executing this action.
   * @throws NoSuchLocationException if a location with this name does not exist.
   */
  public Set<GeoLocationName> listNames(long gid)
      throws GeoServiceException, NoSuchLocationException;

  /**
   * Lists all continents in a specific language.
   *
   * @param lang the language's iso identifier.
   * @return a set of {@link GeoLocationName}.
   * @throws GeoServiceException if an error occurs while executing this action.
   */
  public Set<GeoLocationName> listContinents(String lang)
      throws GeoServiceException;

  /**
   * Lists all continents in the default language.
   *
   * @return a set of {@link GeoLocationName}.
   * @throws GeoServiceException if an error occurs while executing this action.
   */
  public Set<GeoLocationName> listContinents()
      throws GeoServiceException;

  /**
   * Lists the continents by name in a specific language, that
   * start with a given prefix.
   *
   * @param prefix a name prefix.
   * @param lang   the language's iso identifier.
   * @return a set of {@link GeoLocationName}.
   * @throws GeoServiceException if an error occurs while executing this action.
   */
  public Set<GeoLocationName> listContinentsByName(String prefix, String lang)
      throws GeoServiceException;

  /**
   * Lists the continents by name in the default language, that
   * start with a given prefix.
   *
   * @param prefix a name prefix.
   * @return a set of {@link GeoLocationName}.
   * @throws GeoServiceException if an error occurs while executing this action.
   */
  public Set<GeoLocationName> listContinentsByName(String prefix)
      throws GeoServiceException;

  /**
   * Returns a continent for a given country with the name in the sepcified language.
   *
   * @param gid the region geonames identifier.
   * @param lang the language's iso identifier.
   * @return a {@link GeoLocationName}.
   * @throws GeoServiceException     if an error occurs while executing this action.
   * @throws NoSuchLocationException if a location with this name does not exist.
   */
  public GeoLocationName getContinentForCountry(long gid, String lang)
      throws GeoServiceException, NoSuchLocationException;

  /**
   * Returns a continent for a given country with the name in the default language.
   *
   * @param gid the region geonames identifier.
   * @return a {@link GeoLocationName}.
   * @throws GeoServiceException     if an error occurs while executing this action.
   * @throws NoSuchLocationException if a location with this name does not exist.
   */
  public GeoLocationName getContinentForCountry(long gid)
      throws GeoServiceException, NoSuchLocationException;


  /**
   * Lists all countries in a specific language.
   *
   * @param lang the language's iso identifier.
   * @return a set of {@link GeoLocationName}.
   * @throws GeoServiceException if an error occurs while executing this action.
   */
  public Set<GeoLocationName> listCountries(String lang)
      throws GeoServiceException;

  /**
   * Lists all countries in the default language.
   *
   * @return a set of {@link GeoLocationName}.
   * @throws GeoServiceException if an error occurs while executing this action.
   */
  public Set<GeoLocationName> listCountries()
      throws GeoServiceException;

  /**
   * Lists the countries by name in a specific language, that
   * start with a given prefix.
   *
   * @param prefix a name prefix.
   * @param lang   the language's iso identifier.
   * @return a set of {@link GeoLocationName}.
   * @throws GeoServiceException if an error occurs while executing this action.
   */
  public Set<GeoLocationName> listCountriesByName(String prefix, String lang)
      throws GeoServiceException;

  /**
   * Lists the countries by name in the default language, that
   * start with a given prefix.
   *
   * @param prefix a name prefix.
   * @return a set of {@link GeoLocationName}.
   * @throws GeoServiceException if an error occurs while executing this action.
   */
  public Set<GeoLocationName> listCountriesByName(String prefix)
      throws GeoServiceException;

  /**
   * Lists the regions by name in a specific language, that
   * start with a given prefix.
   *
   * @param prefix a name prefix.
   * @param lang   the language's iso identifier.
   * @return a set of {@link GeoLocationName}.
   * @throws GeoServiceException if an error occurs while executing this action.
   */
  public Set<GeoLocationName> listRegionsByName(String prefix, String lang)
      throws GeoServiceException;

  /**
   * Lists the regions by name in the default language, that
   * start with a given prefix.
   *
   * @param prefix a name prefix.
   * @return a set of {@link GeoLocationName}.
   * @throws GeoServiceException if an error occurs while executing this action.
   */
  public Set<GeoLocationName> listRegionsByName(String prefix)
      throws GeoServiceException;

  /**
   * Lists the cities by name in a specific language, that
   * start with a given prefix.
   *
   * @param prefix a name prefix.
   * @param lang   the language's iso identifier.
   * @return a set of {@link GeoLocationName}.
   * @throws GeoServiceException if an error occurs while executing this action.
   */
  public Set<GeoLocationName> listCitiesByName(String prefix, String lang)
      throws GeoServiceException;

  /**
   * Lists the cities by name in the default language, that
   * start with a given prefix.
   *
   * @param prefix a name prefix.
   * @return a set of {@link GeoLocationName}.
   * @throws GeoServiceException if an error occurs while executing this action.
   */
  public Set<GeoLocationName> listCitiesByName(String prefix)
      throws GeoServiceException;

  /**
   * Returns a country for a given region with the name in the specified language.
   *
   * @param gid  the region geonames identifier.
   * @param lang the language's iso identifier.
   * @return a {@link GeoLocationName}.
   * @throws GeoServiceException     if an error occurs while executing this action.
   * @throws NoSuchLocationException if a location with this name does not exist.
   */
  public GeoLocationName getCountryForRegion(long gid, String lang)
      throws GeoServiceException, NoSuchLocationException;

  /**
   * Returns a country for a given region with the name in the default language.
   *
   * @param gid the region geonames identifier.
   * @return a {@link GeoLocationName}.
   * @throws GeoServiceException     if an error occurs while executing this action.
   * @throws NoSuchLocationException if a location with this name does not exist.
   */
  public GeoLocationName getCountryForRegion(long gid)
      throws GeoServiceException, NoSuchLocationException;

  /**
   * Returns a region for a given city with the name in the specified language.
   *
   * @param gid  the city geonames identifier.
   * @param lang the language's iso identifier.
   * @return a {@link GeoLocationName}.
   * @throws GeoServiceException     if an error occurs while executing this action.
   * @throws NoSuchLocationException if a location with this name does not exist.
   */
  public GeoLocationName getRegionForCity(long gid, String lang)
      throws GeoServiceException, NoSuchLocationException;

  /**
   * Returns a region for a given city with the name in the default language.
   *
   * @param gid the city geonames identifier.
   * @return a {@link GeoLocationName}.
   * @throws GeoServiceException     if an error occurs while executing this action.
   * @throws NoSuchLocationException if a location with this name does not exist.
   */
  public GeoLocationName getRegionForCity(long gid)
      throws GeoServiceException, NoSuchLocationException;

  /**
   * Return the geographic location of the given city.
   *
   * @param gid the city's geonames identifier.
   * @return a {@link GeoLocation}.
   * @throws GeoServiceException     if an error occurs while executing this action.
   * @throws NoSuchLocationException if a location with this name does not exist.
   */
  public GeoLocation getCityLocation(long gid)
      throws GeoServiceException, NoSuchLocationException;

  /**
   * Return the geographic location of the given region.
   *
   * @param gid the region's geonames identifier.
   * @return a {@link GeoLocation}.
   * @throws GeoServiceException     if an error occurs while executing this action.
   * @throws NoSuchLocationException if a location with this name does not exist.
   */
  public GeoLocation getRegionLocation(long gid)
      throws GeoServiceException, NoSuchLocationException;

  /**
   * Rebuilds the database.
   *
   * @param caller the calling {@link Agent}.
   * @throws SecurityException if the calling agent is not authentic or authorized.
   */
  public void rebuildDatabase(Agent caller) throws SecurityException;

}//interface GeoService
