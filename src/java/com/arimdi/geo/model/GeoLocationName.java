/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.geo.model;

/**
 * This interface defines the contract for a geo location name.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface GeoLocationName {

  /**
   * Returns the geoname identifier.
   * @return the geoname identifier.
   */
  public Long getIdentifier();

  /**
   * Returns the name in the language associated with this <tt>GeoLocationName</tt>.
   * @return the name.
   */
  public String getName();

  /**
   * Returns a language agnostic shortcut for the name.
   * @return the shortcut.
   */
  public String getShortcut();

  /**
   * Tests if this <tt>GeoLocationName</tt> has a shortcut.
   * @return true if available, false otherwise.
   */
  public boolean hasShortcut();

  /**
   * Returns the language code of this <tt>GeoLocationName</tt>.
   * @return
   */
  public String getLanguage();

}//interface GeoLocationName
