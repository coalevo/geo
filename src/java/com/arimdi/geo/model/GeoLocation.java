/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.geo.model;

/**
 * This interface defines the contract for a geo location.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface GeoLocation {

  /**
   * Returns the geoname identifier.
   * @return the geoname identifier.
   */
  public Long getIdentifier();

  /**
   * Returns the latitude of this <tt>GeoLocation</tt>.
   * @return the latitude.
   */
  public double getLatitude();

  /**
   * Returns the longitude of this <tt>GeoLocation</tt>.
   * @return the longitude.
   */
  public double getLongitude();

}//interface GeoLocation
