/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.geo.impl;

import com.arimdi.geo.model.GeoLocation;

/**
 * This class provides a simple inmutable implementation of {@link GeoLocation}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class GeoLocationImpl implements GeoLocation {

  private final Long m_Identifier;
  private final double m_Latitude;
  private final double m_Longitude;

  GeoLocationImpl(long gid, double latitude, double longitude) {
    m_Identifier = gid;
    m_Latitude = latitude;
    m_Longitude = longitude;
  }//constructor

  public Long getIdentifier() {
    return m_Identifier;
  }//getIdentifier

  public double getLatitude() {
    return m_Latitude;
  }//getLatitude

  public double getLongitude() {
    return m_Longitude;
  }//getLongitude  

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();
    sb.append("GeoLocation");
    sb.append("{").append(m_Identifier);
    sb.append(", ").append(m_Latitude);
    sb.append(", ").append(m_Longitude);
    sb.append('}');
    return sb.toString();
  }//toString
  
}//class GeoLocationImpl
