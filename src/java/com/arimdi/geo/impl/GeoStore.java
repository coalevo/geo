/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.geo.impl;

import com.arimdi.geo.service.GeoServiceConfiguration;
import net.coalevo.datasource.service.DataSourceService;
import net.coalevo.foundation.model.Messages;
import net.coalevo.foundation.util.ConfigurationMediator;
import net.coalevo.foundation.util.ConfigurationUpdateHandler;
import net.coalevo.foundation.util.metatype.MetaTypeDictionary;
import net.coalevo.foundation.util.metatype.MetaTypeDictionaryException;
import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.osgi.framework.BundleContext;
import org.slamb.axamol.library.Library;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import javax.sql.DataSource;
import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.NoSuchElementException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * This class prepares and provides connection access to the backend store.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class GeoStore implements ConfigurationUpdateHandler {

  private static Marker c_LogMarker = MarkerFactory.getMarker(GeoStore.class.getName());

  private Library m_SQLLibrary;
  private DataSource m_DataSource;
  private GenericObjectPool m_ConnectionPool;

  // Concurrency handling for backup/restore
  private AtomicInteger m_NumLeased = new AtomicInteger(0);
  private CountDownLatch m_LeaseLatch;
  private CountDownLatch m_RestoreLatch;

  //Managers
  GeoManager m_GeoManager;

  //l10n
  private Messages m_BundleMessages;

  private String m_DefaultLanguage = "def";

  private File m_ImportDir;

  //*** Activation and configuration handling ***//

  public boolean activate(BundleContext bc) {

    m_BundleMessages = Activator.getBundleMessages();

    //1. Configuration
    ConfigurationMediator cm = Activator.getServices().getConfigMediator();
    MetaTypeDictionary mtd = cm.getConfiguration();
    String ds = "default";
    int cpoolsize = 2;
    String deflang = "def";
    String importdir = "";

    //Retrieve config
    try {
      ds = mtd.getString(GeoServiceConfiguration.DATA_SOURCE_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("GeoStore.configuration.exception", "attribute", GeoServiceConfiguration.DATA_SOURCE_KEY),
          ex
      );
    }
    try {
      cpoolsize = mtd.getInteger(GeoServiceConfiguration.CONNECTION_POOLSIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("GeoStore.configuration.exception", "attribute", GeoServiceConfiguration.CONNECTION_POOLSIZE_KEY),
          ex
      );
    }
    try {
      deflang = mtd.getString(GeoServiceConfiguration.DEFAULT_LANGUAGE_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("GeoStore.configuration.exception", "attribute", GeoServiceConfiguration.DEFAULT_LANGUAGE_KEY),
          ex
      );
    }

    try {
      importdir = mtd.getString(GeoServiceConfiguration.IMPORT_DIR_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("GeoStore.configuration.exception", "attribute", GeoServiceConfiguration.IMPORT_DIR_KEY),
          ex
      );
    }

    //default lang check
    if (deflang == null || deflang.length() == 0) {
      m_DefaultLanguage = "def";
    } else {
      m_DefaultLanguage = deflang;
    }

    if (importdir != null && importdir.length() > 0) {
      m_ImportDir = new File(importdir);
      if (!m_ImportDir.exists() && m_ImportDir.canRead()) {
        m_ImportDir = null;
      } else {
        File[] files = m_ImportDir.listFiles();
        int found = 0;
        for (File f : files) {
          if ("continents.txt".equals(f.getName())) {
            found++;
          }
          if ("timeZones.txt".equals(f.getName())) {
            found++;
          }
          if ("cities1000.zip".equals(f.getName())) {
            found++;
          }
          if ("countryInfo.txt".equals(f.getName())) {
            found++;
          }
          if ("allCountries.zip".equals(f.getName())) {
            found++;
          }
          if ("alternateNames.zip".equals(f.getName())) {
            found++;
          }
        }//end for
        if(found != 6) {
          Activator.log().error(
              c_LogMarker,
              m_BundleMessages.get("GeoStore.configuration.missingfiles")
          );
          m_ImportDir = null;
        }
      }
    }
    //2. Connection Pool
    GenericObjectPool.Config poolcfg = new GenericObjectPool.Config();
    poolcfg.maxActive = cpoolsize;
    poolcfg.maxIdle = cpoolsize;
    poolcfg.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_BLOCK;
    poolcfg.testOnBorrow = true;
    m_ConnectionPool = new GenericObjectPool(new ConnectionFactory(), poolcfg);

    //3. prepare SQL library and datasource
    try {
      m_SQLLibrary =
          new Library(GeoStore.class, "com/arimdi/geo/impl/geostore-sql.xml");
    } catch (Exception ex) {
      Activator.log().error(c_LogMarker, "activate()", ex);
      return false;
    }
    DataSourceService dss =
        Activator.getServices().getDataSourceService(ServiceMediator.WAIT_UNLIMITED);
    try {
      m_DataSource = dss.waitForDataSource(ds, -1);
    } catch (NoSuchElementException nse) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("GeoStore.datasource.error", "source", ds),
          nse
      );
      return false;
    }

    //4. prepare source and schema
    prepareDataSource();
    Activator.log().info(c_LogMarker,
        m_BundleMessages.get("GeoStore.datasource.info", "source", m_DataSource.toString())
    );

    //5. Managers
    m_GeoManager = new GeoManager(this);

    //6. Register update listener
    cm.addUpdateHandler(this);

    return true;
  }//activate

  public synchronized boolean deactivate() {

    //1. sync and clear caches
    if (m_GeoManager != null) {
      //will sync
      m_GeoManager.clearCaches();
      m_GeoManager = null;
    }

    //2. close all leased connections
    if (m_ConnectionPool != null) {
      try {
        m_ConnectionPool.close();
      } catch (Exception e) {
        Activator.log().error(c_LogMarker, "deactivate()", e);
      }
    }

    m_DataSource = null;
    m_SQLLibrary = null;
    m_ConnectionPool = null;
    m_BundleMessages = null;
    return true;
  }//deactivate


  public void update(MetaTypeDictionary mtd) {
    if(mtd == null) {
      return;
    }
    //1. Configure from persistent configuration
    int cpoolsize = 2;
    String deflang = "def";
    String importdir = "";

    //Retrieve config
    try {
      cpoolsize = mtd.getInteger(GeoServiceConfiguration.CONNECTION_POOLSIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("GeoStore.configuration.exception", "attribute", GeoServiceConfiguration.CONNECTION_POOLSIZE_KEY),
          ex
      );
    }
    try {
      deflang = mtd.getString(GeoServiceConfiguration.DEFAULT_LANGUAGE_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("GeoStore.configuration.exception", "attribute", GeoServiceConfiguration.DEFAULT_LANGUAGE_KEY),
          ex
      );
    }
    try {
      importdir = mtd.getString(GeoServiceConfiguration.IMPORT_DIR_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("GeoStore.configuration.exception", "attribute", GeoServiceConfiguration.IMPORT_DIR_KEY),
          ex
      );
    }

    //default lang check
    if (deflang == null || deflang.length() == 0) {
      m_DefaultLanguage = "def";
    } else {
      m_DefaultLanguage = deflang;
    }

    if (importdir != null && importdir.length() > 0) {
      m_ImportDir = new File(importdir);
      if (!m_ImportDir.exists() && m_ImportDir.canRead()) {
        m_ImportDir = null;
      } else {
        File[] files = m_ImportDir.listFiles();
        int found = 0;
        for (File f : files) {
          if ("continents.txt".equals(f.getName())) {
            found++;
          }
          if ("timeZones.txt".equals(f.getName())) {
            found++;
          }
          if ("cities1000.zip".equals(f.getName())) {
            found++;
          }
          if ("countryInfo.txt".equals(f.getName())) {
            found++;
          }
          if ("allCountries.zip".equals(f.getName())) {
            found++;
          }
          if ("alternateNames.zip".equals(f.getName())) {
            found++;
          }
        }//end for
        if(found != 6) {
          Activator.log().error(
              c_LogMarker,
              m_BundleMessages.get("GeoStore.configuration.missingfiles")
          );
          m_ImportDir = null;
        }
      }
    }
    //Update settings
    m_ConnectionPool.setMaxActive(cpoolsize);
    m_ConnectionPool.setMaxIdle(cpoolsize);   
  }//update

  //*** END: Activation and configuration handling ***//

  //*** Accessors ***//

  GeoManager getGeoManager() {
    return m_GeoManager;
  }//getGeoManager

  String getDefaultLanguage() {
    return m_DefaultLanguage;
  }//getDefaultLanguage

  File getImportDir() {
    return m_ImportDir;
  }//getImportDir

  //*** END: Accessors ***//

  //*** Database handling ***//

  /**
   * Lease a connection to the underlying database.
   *
   * @return a {@link LibraryConnection} instance.
   * @throws Exception if the connection pool fails or a connection cannot be created.
   */
  public LibraryConnection leaseConnection()
      throws Exception {
    //If a lease latch is set, wait for countdown
    if (m_LeaseLatch != null) {
      m_LeaseLatch.await();
    }
    LibraryConnection lc = (LibraryConnection) m_ConnectionPool.borrowObject();
    m_NumLeased.addAndGet(1);
    return lc;
  }//leaseConnection

  public void releaseConnection(LibraryConnection lc) {
    try {
      m_ConnectionPool.returnObject(lc);
      if (m_NumLeased.decrementAndGet() == 0 && m_RestoreLatch != null) {
        m_RestoreLatch.countDown();
      }
    } catch (Exception e) {
      Activator.log().error(c_LogMarker, "releaseConnection()", e);
    }
  }//releaseConnection

  private synchronized void prepareDataSource() {
    LibraryConnection lc = null;
    try {
      lc = leaseConnection();
      //1. check select
      try {
        lc.executeQuery("existsSchema", null);
      } catch (SQLException ex) {
        rebuildDatabase(lc);
      }
    } catch (Exception ex) {
      Activator.log().error(c_LogMarker, "prepareDataSource()", ex);
    } finally {
      releaseConnection(lc);
    }
  }//prepareDataSource

  private boolean createSchema(LibraryConnection lc) {

    try {
      //1. Create geo database
      lc.executeCreate("createGeoSchema");
      //1.1 Continents
      lc.executeCreate("createContinents");
      //1.2 Time Zones
      lc.executeCreate("createTimeZones");
      //1.3 Countries
      lc.executeCreate("createCountries");
      lc.executeCreate("createCountryContinentsIndex");
      //1.4 Regions
      lc.executeCreate("createRegions");
      lc.executeCreate("createRegionCountryCodeIndex");
      lc.executeCreate("createRegionCodeIndex");
      //1.5 Cities
      lc.executeCreate("createCities");
      lc.executeCreate("createCityCountryCodeIndex");
      lc.executeCreate("createCityRegionCodeIndex");
      //1.6 Names
      lc.executeCreate("createNames");
      lc.executeCreate("createNameIdentifierIndex");
      lc.executeCreate("createNameIndex");
      lc.executeCreate("createNameLanguageIndex");

    } catch (Exception ex) {
      Activator.log().error(c_LogMarker, "createSchema()", ex);
    }
    Activator.log().info(c_LogMarker, m_BundleMessages.get("GeoStore.datasource.schema"));
    return true;
  }//createSchema

  void rebuildDatabase(LibraryConnection lc) {
    try {
      if(m_ImportDir != null) {
        lc.executeCreate("removeGeoSchema");
        createSchema(lc);
        GeoImport gi = new GeoImport(this);
        gi.runImport(lc);
      }
    } catch (Exception ex) {
      Activator.log().error(c_LogMarker, "rebuildDatabase()", ex);
    }
  }//rebuildDatabase

  private class ConnectionFactory
      extends BasePoolableObjectFactory {

    public Object makeObject() throws Exception {
      Connection c = m_DataSource.getConnection();
      c.setAutoCommit(true);
      return new LibraryConnection(m_SQLLibrary, c);
    }//makeObject

    public boolean validateObject(Object obj) {
      final LibraryConnection lc = (LibraryConnection) obj;
      try {
        lc.executeQuery("validate", null);
      } catch (Exception ex) {
        return false;
      }
      return true;
    }//validateObject

    public void destroyObject(Object obj) {
      final LibraryConnection lc = (LibraryConnection) obj;
      if (!lc.isClosed()) {
        SqlUtils.close(lc);
      }
    }//destroyObject

  }//ConnectionFactory

  //*** END: Database Handling ***//

}//class GeoStore
