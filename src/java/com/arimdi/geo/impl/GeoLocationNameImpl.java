/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.geo.impl;

import com.arimdi.geo.model.GeoLocationName;

/**
 * This class provides a simple inmutable implementation of {@link GeoLocationName}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
    class GeoLocationNameImpl implements GeoLocationName {

  private final Long m_Identifier;
  private final String m_Name;
  private final String m_Shortcut;
  private final String m_Language;

  GeoLocationNameImpl(Long identifier, String language, String name) {
    m_Identifier = identifier;
    m_Language = language;
    m_Name = name;
    m_Shortcut = null;
  }//GeoLocationName

  GeoLocationNameImpl(Long identifier, String language, String name, String sc) {
    m_Identifier = identifier;
    m_Language = language;
    m_Name = name;
    m_Shortcut = sc;
  }//GeoLocationName

  public Long getIdentifier() {
    return m_Identifier;
  }//getIdentifier

  public String getLanguage() {
    return m_Language;
  }//getLanguage

  public String getName() {
    return m_Name;
  }//getName

  public String getShortcut() {
    return m_Shortcut;
  }//getShortcut

  public boolean hasShortcut() {
    return m_Shortcut != null && m_Shortcut.length() > 0;
  }//hasShortcut

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();
    sb.append("GeoLocationName");
    sb.append("{m_Identifier=").append(m_Identifier);
    sb.append(", m_Name='").append(m_Name).append('\'');
    sb.append(", m_Language='").append(m_Language).append('\'');
    sb.append('}');
    return sb.toString();
  }//toString
  
}//class GeoLocationNameImpl
