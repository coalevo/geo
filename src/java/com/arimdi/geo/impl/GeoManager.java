/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.geo.impl;

import com.arimdi.geo.model.GeoLocation;
import com.arimdi.geo.model.GeoLocationName;
import com.arimdi.geo.model.GeoServiceException;
import com.arimdi.geo.model.NoSuchLocationException;
import org.slamb.axamol.library.LibraryConnection;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * This class provides a manager of the geo data, mediating between the
 * service and the store.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class GeoManager {

  private Marker m_LogMarker = MarkerFactory.getMarker(GeoManager.class.getName());
  private GeoStore m_Store;

  GeoManager(GeoStore store) {
    m_Store = store;
  }//constructor

  void clearCaches() {

  }//clearCaches

  long getIdentifierForName(String name)
      throws GeoServiceException, NoSuchLocationException {

    HashMap<String, String> params = new HashMap<String, String>();
    params.put("name", name);
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      ResultSet rs = lc.executeQuery("getIdentifierForName", params);
      if (rs.next()) {
        return rs.getLong(1);
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "getSelection()", ex);
      throw new GeoServiceException(Activator.getBundleMessages().get("GeoManager.error.identifierforname"), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
    throw new NoSuchLocationException(name);
  }//getIdentifierForName

  public GeoLocationName getName(long gid, String lang)
      throws GeoServiceException, NoSuchLocationException {

    HashMap<String, String> params = new HashMap<String, String>();
    params.put("gid", Long.toString(gid));
    params.put("lang", lang);

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      ResultSet rs = lc.executeQuery("getNameForIdentifier", params);
      if (rs.next()) {
        return new GeoLocationNameImpl(
            gid,
            lang,
            rs.getString(1)
        );
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "getName()::" + params.toString(), ex);
      throw new GeoServiceException(Activator.getBundleMessages().get("GeoManager.error.nameforidentifier"), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
    throw new NoSuchLocationException(params.toString());
  }//getName

  public GeoLocationName getName(long gid)
      throws GeoServiceException, NoSuchLocationException {

    return getName(gid, m_Store.getDefaultLanguage());
  }//getName

  public Set<GeoLocationName> listNames(long gid)
      throws GeoServiceException {

    Set<GeoLocationName> s = new HashSet<GeoLocationName>();
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("gid", Long.toString(gid));

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      ResultSet rs = lc.executeQuery("listNames", params);
      while (rs.next()) {
        s.add(new GeoLocationNameImpl(
            gid,
            rs.getString(2),
            rs.getString(1)
        ));
      }
      return s;
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "listNames()::" + params.toString(), ex);
      throw new GeoServiceException(Activator.getBundleMessages().get("GeoManager.error.listnames"), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
  }//listNames

   public Set<GeoLocationName> listContinents(String lang)
      throws GeoServiceException {

    Set<GeoLocationName> s = new HashSet<GeoLocationName>();
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("lang", lang);

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      ResultSet rs = lc.executeQuery("listContinents", params);
      while (rs.next()) {
        s.add(new GeoLocationNameImpl(
            rs.getLong(1),
            lang,
            rs.getString(2),
            rs.getString(3)
        ));
      }
      return s;
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "listContinents()::" + params.toString(), ex);
      throw new GeoServiceException(Activator.getBundleMessages().get("GeoManager.error.listcontinents"), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }

  }//listContinents

  public Set<GeoLocationName> listContinents()
      throws GeoServiceException {
    return listContinents(m_Store.getDefaultLanguage());
  }//listContinents

  public Set<GeoLocationName> listContinentsByName(String prefix, String lang)
      throws GeoServiceException {

    Set<GeoLocationName> s = new HashSet<GeoLocationName>();
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("prefix", prefix.toLowerCase() + "%");
    params.put("lang", lang);

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      ResultSet rs = lc.executeQuery("listContinentsByName", params);
      while (rs.next()) {
        s.add(new GeoLocationNameImpl(
            rs.getLong(1),
            lang,
            rs.getString(2),
            rs.getString(3)
        ));
      }
      return s;
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "listContinentsByName()::" + params.toString(), ex);
      throw new GeoServiceException(Activator.getBundleMessages().get("GeoManager.error.listcontinentsbyname"), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
  }//listContinentsByName

  public Set<GeoLocationName> listContinentsByName(String prefix)
      throws GeoServiceException {
    return listContinentsByName(prefix, m_Store.getDefaultLanguage());
  }//listContinentsByName

  public GeoLocationName getContinentForCountry(long gid, String lang)
      throws GeoServiceException {
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("gid", Long.toString(gid));
    params.put("lang", lang);

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      ResultSet rs = lc.executeQuery("getContinentForCountry", params);
      if (rs.next()) {
        return new GeoLocationNameImpl(
            rs.getLong(1),
            lang,
            rs.getString(2)
        );
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "getContinentForCountry()::" + params.toString(), ex);
      throw new GeoServiceException(Activator.getBundleMessages().get("GeoManager.error.continentforcountry"), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
    throw new NoSuchLocationException(params.toString());
  }//getContinentForCountry

  public GeoLocationName getContinentForCountry(long gid)
      throws GeoServiceException {
    return getContinentForCountry(gid, m_Store.getDefaultLanguage());
  }//getContinentForCountry

  public Set<GeoLocationName> listCountries(String lang)
      throws GeoServiceException {

    Set<GeoLocationName> s = new HashSet<GeoLocationName>();
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("lang", lang);

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      ResultSet rs = lc.executeQuery("listCountries", params);
      while (rs.next()) {
        s.add(new GeoLocationNameImpl(
            rs.getLong(1),
            lang,
            rs.getString(2),
            rs.getString(3)
        ));
      }
      return s;
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "listCountries()::" + params.toString(), ex);
      throw new GeoServiceException(Activator.getBundleMessages().get("GeoManager.error.listcountries"), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
  }//listCountries

  public Set<GeoLocationName> listCountries()
      throws GeoServiceException {
    return listCountries(m_Store.getDefaultLanguage());
  }//listCountries

  public Set<GeoLocationName> listCountriesByName(String prefix, String lang)
      throws GeoServiceException {

    Set<GeoLocationName> s = new HashSet<GeoLocationName>();
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("prefix", prefix.toLowerCase() + "%");
    params.put("lang", lang);

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      ResultSet rs = lc.executeQuery("listCountriesByName", params);
      while (rs.next()) {
        s.add(new GeoLocationNameImpl(
            rs.getLong(1),
            lang,
            rs.getString(2),
            rs.getString(3)
        ));
      }
      return s;
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "listCountriesByName()::" + params.toString(), ex);
      throw new GeoServiceException(Activator.getBundleMessages().get("GeoManager.error.listcountriesbyname"), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
  }//listCountriesByName

  public Set<GeoLocationName> listCountriesByName(String prefix)
      throws GeoServiceException {
    return listCountriesByName(prefix, m_Store.getDefaultLanguage());
  }//listCountriesByName

  public Set<GeoLocationName> listRegionsByName(String prefix, String lang)
      throws GeoServiceException {
    Set<GeoLocationName> s = new HashSet<GeoLocationName>();
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("prefix", prefix.toLowerCase() + "%");
    params.put("lang", lang);

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      ResultSet rs = lc.executeQuery("listRegionsByName", params);
      while (rs.next()) {
        s.add(new GeoLocationNameImpl(
            rs.getLong(1),
            lang,
            rs.getString(2),
            rs.getString(3)
        ));
      }
      return s;
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "listRegionsByName()::" + params.toString(), ex);
      throw new GeoServiceException(Activator.getBundleMessages().get("GeoManager.error.listregionsbyname"), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
  }//listRegionsByName

  public Set<GeoLocationName> listRegionsByName(String prefix)
      throws GeoServiceException {
    return listRegionsByName(prefix, m_Store.getDefaultLanguage());
  }//listRegionsByName

  public Set<GeoLocationName> listCitiesByName(String prefix, String lang)
      throws GeoServiceException {

    Set<GeoLocationName> s = new HashSet<GeoLocationName>();
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("prefix", prefix.toLowerCase() + "%");   
    params.put("lang", lang);

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      ResultSet rs = lc.executeQuery("listCitiesByName", params);
      while (rs.next()) {
        s.add(new GeoLocationNameImpl(
            rs.getLong(1),
            lang,
            rs.getString(2)
        ));
      }
      return s;
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "listCitiesByName()::" + params.toString(), ex);
      throw new GeoServiceException(Activator.getBundleMessages().get("GeoManager.error.listcitiesbyname"), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
  }//listCitiesByName

  public Set<GeoLocationName> listCitiesByName(String prefix)
      throws GeoServiceException {
    return listCitiesByName(prefix, m_Store.getDefaultLanguage());
  }//listCitiesByName

  public GeoLocationName getCountryForRegion(long gid, String lang)
      throws GeoServiceException {
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("gid", Long.toString(gid));
    params.put("lang", lang);

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      ResultSet rs = lc.executeQuery("getCountryForRegion", params);
      if (rs.next()) {
        return new GeoLocationNameImpl(
            rs.getLong(1),
            lang,
            rs.getString(2)
        );
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "getCountryForRegion()::" + params.toString(), ex);
      throw new GeoServiceException(Activator.getBundleMessages().get("GeoManager.error.countryforregion"), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
    throw new NoSuchLocationException(params.toString());
  }//getCountryForRegion

  public GeoLocationName getCountryForRegion(long gid)
      throws GeoServiceException {
    return getCountryForRegion(gid, m_Store.getDefaultLanguage());
  }//getCountryForRegion

  public GeoLocationName getRegionForCity(long gid, String lang)
      throws GeoServiceException {
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("gid", Long.toString(gid));
    params.put("lang", lang);

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      ResultSet rs = lc.executeQuery("getRegionForCity", params);
      if (rs.next()) {
        return new GeoLocationNameImpl(
            rs.getLong(1),
            lang,
            rs.getString(2)
        );
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "getRegionForCity()::" + params.toString(), ex);
      throw new GeoServiceException(Activator.getBundleMessages().get("GeoManager.error.regionforcity"), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
    throw new NoSuchLocationException(params.toString());
  }//getRegionForCity

  public GeoLocationName getRegionForCity(long gid)
      throws GeoServiceException {
    return getRegionForCity(gid, m_Store.getDefaultLanguage());
  }//getRegionForCity

  public GeoLocation getCityLocation(long gid)
      throws GeoServiceException {
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("gid", Long.toString(gid));

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      ResultSet rs = lc.executeQuery("getCityLocation", params);
      if (rs.next()) {
        return new GeoLocationImpl(
            gid,
            rs.getDouble(1),
            rs.getDouble(2)
        );
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "getCityLocation()::" + params.toString(), ex);
      throw new GeoServiceException(Activator.getBundleMessages().get("GeoManager.error.citylocation"), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
    throw new NoSuchLocationException(params.toString());
  }//getCityLocation

  public GeoLocation getRegionLocation(long gid)
      throws GeoServiceException {

    HashMap<String, String> params = new HashMap<String, String>();
    params.put("gid", Long.toString(gid));

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      ResultSet rs = lc.executeQuery("getRegionLocation", params);
      if (rs.next()) {
        return new GeoLocationImpl(
            gid,
            rs.getDouble(1),
            rs.getDouble(2)
        );
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "getRegionLocation()::" + params.toString(), ex);
      throw new GeoServiceException(Activator.getBundleMessages().get("GeoManager.error.regionlocation"), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
    throw new NoSuchLocationException(params.toString());
  }//getRegionLocation

  public void rebuildDatabase()
      throws GeoServiceException {
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      m_Store.rebuildDatabase(lc);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "rebuildDatabase()", ex);
      throw new GeoServiceException(Activator.getBundleMessages().get("GeoManager.error.rebuilddatabase"), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
  }//rebuildDatabase

}//class GeoManager
