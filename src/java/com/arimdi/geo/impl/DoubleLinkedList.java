/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/s
package com.arimdi.geo.impl;

/**
 * Implements a double linked list.
 * <p/>
 * Taken from: http://www.javaworld.com/javaworld/jw-02-2001/jw-0216-ternary.html
 *
 * @author Wally Flint
 * @version @version@ (@date@)
 */
import java.util.*;

class DoubleLinkedList
    extends AbstractSet
    implements Set {

	private DLLNode head, last;
	private int size = 0;

	public void addFirst(Object data) {
		DLLNode newNode = new DLLNode();
		newNode.data = data;
		if(size==0) {
			head = newNode;
			last = head;
		} else {
			newNode.nextNode = head;
			head.previousNode = newNode;
			head = newNode;
		}
		size++;
	}

	public void addLast(Object data) {
		DLLNode newNode = new DLLNode();
		newNode.data = data;
		if(size==0) {
			head = newNode;
		} else {
			last.nextNode = newNode;
			newNode.previousNode = last;
		}
		last = newNode;
		size++;
	}

	public void removeFirst() {
		if(size <= 1) {
			head = null;
			last = null;
		} else {
			DLLNode oldHead = head;
			head = oldHead.nextNode;
			oldHead.nextNode = null;
			head.previousNode = null;
		}
		size--;
	}

	public void removeLast() {
		if(size <= 1) {
			head = null;
			last = null;
		} else {
			last = last.previousNode;
			last.nextNode.previousNode = null;
			last.nextNode = null;
		}
		size--;
	}

	public int size() {
		return size;
	}

  public boolean isEmpty() {
    return size() == 0;
  }

  public void clear() {
		DLLNode currentNode = last;
		DLLNode tempNode;
		while(currentNode != null) {
			tempNode = currentNode.previousNode;
			currentNode.nextNode = null;
			currentNode.previousNode = null;
			currentNode.data = null;
			currentNode = tempNode;
		}
		last = null;
		head = null;
		size = 0;
	}

	private class DLLNode {
		protected DLLNode nextNode, previousNode;
		protected Object data;
	}

	public DLLIterator iterator() {
		return new DLLIterator();
	}

	public class DLLIterator implements Iterator {

		private DLLNode currentPreviousNode = null;
		private DLLNode currentNextNode = head;

		public boolean hasNext() {
			if(currentNextNode == null) {
				return false;
			} else {
				return(currentNextNode != null);
			}
		}

		public boolean hasPrevious() {
			if(currentPreviousNode == null) {
				return false;
			} else {
				return (currentPreviousNode != null);
			}
		}

		public Object next() {
			if(currentNextNode == null) throw new NoSuchElementException("Attempt to retrieve next value from " +
				"DoubleLinkedList after all values have already been retrieved. Verify hasNext method returns true " +
				"before calling next method.");
			Object data = currentNextNode.data;
			DLLNode tempNode = currentNextNode;
			currentNextNode = currentNextNode.nextNode;
			currentPreviousNode = tempNode;
			return data;
		}

    public void remove() {
      throw new UnsupportedOperationException();
    }//remove

    public Object previous() {
			if(currentPreviousNode == null) throw new NoSuchElementException("Attempt to retrieve previous value from " +
				"head node of DoubleLinkedList. Verify hasPrevious method returns true " +
				"before calling previous method.");
			Object data = currentPreviousNode.data;
			DLLNode tempNode = currentPreviousNode;
			currentPreviousNode = currentPreviousNode.previousNode;
			currentNextNode = tempNode;
			return data;
		}

		public void resetToBeginning() {
			currentNextNode = head;
			currentPreviousNode = null;
		}

		public void resetToEnd() {
			currentNextNode = null;
			currentPreviousNode = last;
		}
	}

}//class DoubleLinkedList
