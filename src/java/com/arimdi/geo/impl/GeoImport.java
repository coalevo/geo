/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.geo.impl;

import net.coalevo.foundation.model.Messages;
import org.slamb.axamol.library.LibraryConnection;
import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipInputStream;

/**
 * Class that imports data from files that can be obtained
 * from <a href="http://download.geonames.org/export/dump/">geonames.org</a>
 * under the following license:
 * This work is licensed under a Creative Commons Attribution 3.0 License,
 * see http://creativecommons.org/licenses/by/3.0/
 * The Data is provided "as is" without warranty or any representation of accuracy,
 * timeliness or completeness.
 *
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 */
class GeoImport {

  //Logging
  private Marker m_LogMarker = MarkerFactory.getMarker(GeoImport.class.getName());
  private Logger m_Log = Activator.log();
  private Messages m_Messages = Activator.getBundleMessages();

  //Intermediates
  private Map<Long, String> m_GIDSDefault = new HashMap<Long, String>();
  private Map<String, Integer> m_TimeZones = new HashMap<String, Integer>();
  private Map<String, String> m_Capitals = new HashMap<String, String>();
  private Map<String, String> m_CCap = new HashMap<String, String>();
  private Map<String, String> m_Params = new HashMap<String, String>();

  //Store & Import Directory
  private GeoStore m_Store;
  private File m_ImportDir;

  public GeoImport(GeoStore store) {
    m_Store = store;
    m_ImportDir = m_Store.getImportDir();
  }//GeoImport

  /**
   * Imports continents from the <tt>continents.txt</tt>
   * file in the import directory.</br>
   * Expected fields are:
   * 1 geonameid
   * 2 short
   * 3 name
   *
   * @param lc the library connection.
   * @throws IOException if the file cannot be read.
   */
  public void importContinents(LibraryConnection lc) throws IOException {
    m_Log.info(m_LogMarker, m_Messages.get("GeoImport.start", "import" , "continents"));

    //1. Input file
    File input = new File(m_ImportDir, "continents.txt");
    BufferedReader reader = getReader(input);
    //skip header
    reader.readLine();
    boolean done = false;
    int count = 0;
    try {
      do {
        String line = reader.readLine();
        if (line == null) {
          done = true;
        } else if (line.startsWith("#")) {
          continue;
        } else {
          String[] fields = line.split("\t");
          long gid = Long.valueOf(fields[0]);
          m_GIDSDefault.put(gid, fields[2]);

          //insert
          m_Params.clear();
          m_Params.put("gid", Long.toString(gid));
          m_Params.put("sc", fields[1]);
          lc.executeUpdate("insertContinent", m_Params);

          //Count
          count++;
        }
      } while (!done);
    } catch (Exception ex) {
      m_Log.error(m_LogMarker, "importContinents::" + m_Params.toString(), ex);
    }
    reader.close();
    m_Log.info(m_LogMarker, m_Messages.get("GeoImport.end",  "import" , "continents", "count", Long.toString(count)));
  }//importContinents

  /**
   * Import time zones from the <tt>timeZones.txt</tt> file
   * in the import directory.<br/>
   * Expected fields are:
   * 1 TimeZoneId
   * 2 GMT offset 1. Jan 2009
   * 3 DST offset 1. Jul 2009
   *
   * @param lc the library connection.
   * @throws IOException if the file cannot be read.
   */
  public void importTimeZones(LibraryConnection lc) throws IOException {
    m_Log.info(m_LogMarker, m_Messages.get("GeoImport.start", "import" , "timezones"));

    //1. Input file
    File input = new File(m_ImportDir, "timeZones.txt");
    BufferedReader reader = getReader(input);

    //skip header
    reader.readLine();

    boolean done = false;
    int id = 0;
    try {
      do {
        String line = reader.readLine();
        if (line == null) {
          done = true;
        } else if (line.startsWith("#")) {
          continue;
        } else {
          String[] fields = line.split("\t");
          /*
          * 1 TimeZoneId
          * 2 GMT offset 1. Jan 2009
          * 3 DST offset 1. Jul 2009
          */
          // 1. Store mapping
          String name = fields[0];
          int tzid = id++;
          m_TimeZones.put(name, tzid);
          //2. Insert
          m_Params.clear();
          m_Params.put("tzid", Integer.toString(tzid));
          m_Params.put("name", name);
          m_Params.put("gmtoff", fields[1]);
          m_Params.put("dstoff", fields[2]);
          lc.executeUpdate("insertTimeZone", m_Params);
        }
      } while (!done);
    } catch (Exception ex) {
      m_Log.error(m_LogMarker, "importTimeZones::" + m_Params.toString(), ex);
    }
    reader.close();
    m_Log.info(m_LogMarker, m_Messages.get("GeoImport.end", "import", "timezones","count", Integer.toString(id)));
  }//importTimeZones

  /**
   * Import citites from the <tt>cities1000.zip</tt> file
   * in the import directory.<br/>
   * Expected fields are:
   * 1  geonameid         : integer id of record in geonames database
   * 2  name              : name of geographical point (utf8) varchar(200)
   * 3  asciiname         : name of geographical point in plain ascii characters, varchar(200)
   * 4  alternatenames    : alternatenames, comma separated varchar(5000)
   * 5  latitude          : latitude in decimal degrees (wgs84)
   * 6  longitude         : longitude in decimal degrees (wgs84)
   * 7  feature class     : see http://www.geonames.org/export/codes.html, char(1)
   * 8  feature code      : see http://www.geonames.org/export/codes.html, varchar(10)
   * 9  country code      : ISO-3166 2-letter country code, 2 characters
   * 10  cc2               : alternate country codes, comma separated, ISO-3166 2-letter country code, 60 characters
   * 11  admin1 code       : fipscode (subject to change to iso code), isocode for the us and ch, see file admin1Codes.txt for display names of this code; varchar(20)
   * 12  admin2 code       : code for the second administrative division, a county in the US, see file admin2Codes.txt; varchar(80)
   * 13  admin3 code       : code for third level administrative division, varchar(20)
   * 14  admin4 code       : code for fourth level administrative division, varchar(20)
   * 15  population        : bigint (4 byte int)
   * 16  elevation         : in meters, integer
   * 17  gtopo30           : average elevation of 30'x30' (ca 900mx900m) area in meters, integer
   * 18  timezone          : the timezone id (see file timeZone.txt)
   * 19  modification date : date of last modification in yyyy-MM-dd format
   *
   * @param lc the library connection.
   * @throws IOException if the file cannot be read.
   */
  public void importCities(LibraryConnection lc) throws IOException {
    m_Log.info(m_LogMarker, m_Messages.get("GeoImport.start", "import" , "cities"));

    //1. Input file
    File input = new File(m_ImportDir, "cities1000.zip");
    ZipInputStream zis = new ZipInputStream(new FileInputStream(input));
    zis.getNextEntry();
    BufferedReader reader = new BufferedReader(
        new InputStreamReader(
            zis,
            "UTF-8"
        )
    );

    long count = 0;
    boolean done = false;
    try {
      do {
        String line = reader.readLine();
        if (line == null) {
          done = true;
        } else if (line.startsWith("#")) {
          continue;
        } else {
          String[] fields = line.split("\t");

          //1. Track GID
          long gid = Long.valueOf(fields[0]);
          m_GIDSDefault.put(gid, fields[1]);
          //2. Insert
          m_Params.clear();
          m_Params.put("gid", Long.toString(gid));
          m_Params.put("cc", fields[8]);
          m_Params.put("rc", fields[10]);
          m_Params.put("lat", fields[4]);
          m_Params.put("long", fields[5]);
          m_Params.put("tz", Integer.toString(m_TimeZones.get(fields[17])));
          lc.executeUpdate("insertCity", m_Params);

          //3. Cache capitals
          if ("PPLC".equals(fields[7])) {
            String cap = fields[1];
            m_Capitals.put(cap, fields[0]);
            m_CCap.put(fields[8], cap);
          }
          count++;
        }
      } while (!done);
    } catch (Exception ex) {
      m_Log.error(m_LogMarker, "importCities::" + m_Params.toString(), ex);
    }
    reader.close();

    //Print log
    m_Log.info(m_LogMarker, m_Messages.get("GeoImport.end",  "import" , "cities", "count", Long.toString(count)));
/*
    m_Log.info(m_LogMarker, m_Messages.get(
        "GeoImport.endcities",
        "count", Long.toString(count),
        "capitals", Long.toString(m_Capitals.size())
    )
    );
    */
  }//importCities

  /**
   * Import countries from the <tt>countryInfo.txt</tt> file
   * in the import directory.<br/>
   * <p/>
   * Expected fields are:
   * #ISO	ISO3	ISO-Numeric	fips	Country	Capital	Area(in sq km)	Population	Continent	tld	CurrencyCode	CurrencyName	Phone	Postal Code Format	Postal Code Regex	Languages	geonameid	neighbours	EquivalentFipsCode
   * 1  ISO
   * 2  ISO3
   * 3  ISO-Numeric
   * 4  fips
   * 5  Country
   * 6  Capital
   * 7  Area(in sq km)
   * 8  Population
   * 9  Continent
   * 10 tld
   * 11 CurrencyCode
   * 12 CurrencyName
   * 13 Phone
   * 14 Postal Code Format
   * 15 Postal Code Regex
   * 16 Languages
   * 17 geonameid
   * 18 neighbours
   * 19 EquivalentFipsCode
   *
   * @param lc the library connection.
   * @throws IOException if the file cannot be read.
   */
  public void importCountries(LibraryConnection lc) throws IOException {
    m_Log.info(m_LogMarker, m_Messages.get("GeoImport.start", "import" , "countries"));

    //1. input file
    File input = new File(m_ImportDir, "countryInfo.txt");
    BufferedReader reader = getReader(input);

    long count = 0;
    boolean done = false;
    try {
      do {
        String line = reader.readLine();
        if (line == null) {
          done = true;
        } else if (line.startsWith("#")) {
          continue;
        } else {
          String[] fields = line.split("\t");
          if (fields.length < 17) {
            m_Log.error(m_LogMarker, "\n\n==>" + line + "<== \n\n");
            m_Log.error(m_LogMarker, "\n\n " + fields.length + "==>" + Arrays.toString(fields) + "<== \n\n");
            return;
          }
          //1. Track GID
          long gid = Long.valueOf(fields[16]);
          m_GIDSDefault.put(gid, fields[4]);

          //2. Insert country
          String neighb = "";
          if (fields.length > 17) {
            neighb = fields[17];
          }
          String cap = m_Capitals.remove(fields[5]);
          if (cap == null) {
            //Try country fix
            cap = m_Capitals.remove(m_CCap.remove(fields[0]));
            if (cap == null) {
              cap = "0";
              m_Log.debug(m_LogMarker,
                  m_Messages.get(
                      "GeoImport.nocapital",
                      "country", fields[5],
                      "fields", Arrays.toString(fields)
                  )
              );
            }
          }
          m_Params.clear();
          m_Params.put("gid", fields[16]);
          m_Params.put("cc", fields[0]);
          m_Params.put("iso3", fields[1]);
          m_Params.put("capital", cap);
          m_Params.put("continent", fields[8]);
          m_Params.put("languages", fields[15]);
          m_Params.put("neighbours", neighb);
              
          lc.executeUpdate("insertCountry", m_Params);
          count++;
        }
      } while (!done);
    } catch (Exception ex) {
      m_Log.error(m_LogMarker, "importCountries::" + m_Params.toString(), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }

    reader.close();
    m_Log.debug(m_LogMarker, m_Messages.get("GeoImport.unusedcapitals", "capitals", m_Capitals.toString()));
    m_Log.info(m_LogMarker, m_Messages.get("GeoImport.end",  "import" , "countries", "count", Long.toString(count)));

    //Flush capitals cache 
    m_Capitals.clear();
    m_CCap.clear();
  }//importCountries

  /**
   * Import regions from the <tt>allCountries.zip</tt> file
   * in the import directory.<br/>
   * Expected fields are:
   * 1  geonameid         : integer id of record in geonames database
   * 2  name              : name of geographical point (utf8) varchar(200)
   * 3  asciiname         : name of geographical point in plain ascii characters, varchar(200)
   * 4  alternatenames    : alternatenames, comma separated varchar(5000)
   * 5  latitude          : latitude in decimal degrees (wgs84)
   * 6  longitude         : longitude in decimal degrees (wgs84)
   * 7  feature class     : see http://www.geonames.org/export/codes.html, char(1)
   * 8  feature code      : see http://www.geonames.org/export/codes.html, varchar(10)
   * 9  country code      : ISO-3166 2-letter country code, 2 characters
   * 10  cc2               : alternate country codes, comma separated, ISO-3166 2-letter country code, 60 characters
   * 11  admin1 code       : fipscode (subject to change to iso code), isocode for the us and ch, see file admin1Codes.txt for display names of this code; varchar(20)
   * 12  admin2 code       : code for the second administrative division, a county in the US, see file admin2Codes.txt; varchar(80)
   * 13  admin3 code       : code for third level administrative division, varchar(20)
   * 14  admin4 code       : code for fourth level administrative division, varchar(20)
   * 15  population        : bigint (4 byte int)
   * 16  elevation         : in meters, integer
   * 17  gtopo30           : average elevation of 30'x30' (ca 900mx900m) area in meters, integer
   * 18  timezone          : the timezone id (see file timeZone.txt)
   * 19  modification date : date of last modification in yyyy-MM-dd format   *
   *
   * @param lc the library connection.
   * @throws IOException if the file cannot be read.
   */
  public void importRegions(LibraryConnection lc) throws IOException {
    m_Log.info(m_LogMarker, m_Messages.get("GeoImport.start", "import" , "regions"));

    //1. Input file
    File input = new File(m_ImportDir, "allCountries.zip");
    ZipInputStream zis = new ZipInputStream(new FileInputStream(input));
    zis.getNextEntry();
    BufferedReader reader = new BufferedReader(
        new InputStreamReader(
            zis,
            "UTF-8"
        )
    );

    long count = 0;
    boolean done = false;

    try {
      do {
        String line = reader.readLine();
        if (line == null) {
          done = true;
        } else if (line.startsWith("#")) {
          continue;
        } else {
          String[] fields = line.split("\t");
          // Skip not adm1
          if (!"ADM1".equals(fields[7])) {
            continue;
          }

          //1. Track GID
          long gid = Long.valueOf(fields[0]);
          m_GIDSDefault.put(gid, fields[1]);

          //2. Insert Region
          m_Params.clear();
          m_Params.put("gid", fields[0]);
          m_Params.put("cc", fields[8]);
          m_Params.put("rc", fields[10]);
          m_Params.put("lat", fields[4]);
          m_Params.put("long", fields[5]);
          Integer tzid = m_TimeZones.get(fields[17]);
          if(tzid == null) {
            m_Log.error("insertRegion: TimeZone not found or invalid (" + fields[17] + ")::" + m_Params.toString());
            continue;  //skip
          }
          m_Params.put("tz", Integer.toString(tzid));

          lc.executeUpdate("insertRegion", m_Params);

          count++;

        }
      } while (!done);
    } catch (Exception ex) {
      m_Log.error(m_LogMarker, "importRegions::" + m_Params.toString(), ex);
    } 
    reader.close();

    //flush timezones cache
    m_TimeZones.clear();

    m_Log.info(m_LogMarker, m_Messages.get("GeoImport.end",  "import" , "regions", "count", Long.toString(count)));
  }//importRegions

  /**
   * Import names from the <tt>alternateNames.zip</tt> file
   * in the import directory.<br/>
   * Expected fields are:
   * 1  altnameid
   * 2  geonameid
   * 3  isolanguage
   * 4  altname
   * 5  ispreferred
   * 6  isshort
   *
   * @param lc the library connection.
   * @throws IOException if the file cannot be read.
   */
  public void importNames(LibraryConnection lc) throws IOException {
    m_Log.info(m_LogMarker, m_Messages.get("GeoImport.start", "import" , "alternate names"));
    m_Log.debug(m_LogMarker, "importNames()::defaults #=" + m_GIDSDefault.size());

    //1. Input file
    File input = new File(m_ImportDir, "alternateNames.zip");
    ZipInputStream zis = new ZipInputStream(new FileInputStream(input));
    zis.getNextEntry();
    zis.getNextEntry();
    BufferedReader reader = new BufferedReader(
        new InputStreamReader(
            zis,
            "UTF-8"
        )
    );
    long count = 0;
    try {
      //1. Dump Defaults
      Set<Long> gids = m_GIDSDefault.keySet();
      for (Long gid : gids) {
        //1. Insert geo.names
        String name = m_GIDSDefault.get(gid);
        if (name != null && name.length() > 0) {
          if (name.startsWith("Estado de")) {
            name = name.substring(10);
          }
          name = escape(name);
          m_Params.clear();
          m_Params.put("gid", gid.toString());
          m_Params.put("lang", "def");
          m_Params.put("name", name);
          m_Params.put("pref", "0");
          m_Params.put("short", "0");
          lc.executeUpdate("insertName", m_Params);
          count++;
        }//end if
      }

      boolean done = false;
      do {
        String line = reader.readLine();
        if (line == null) {
          done = true;
        } else {
          String[] fields = line.split("\t");
          Long gid = Long.valueOf(fields[1]);
          if (gids.contains(gid)) {
            String pref = "0";
            String sh = "0";

            if (fields.length > 4) {
              if ("1".equals(fields[4])) {
                pref = "1";
              }
            }
            if (fields.length > 5) {
              if ("1".equals(fields[5])) {
                sh = "1";
              }
            }
            //1. Insert geo.names
            m_Params.clear();
            m_Params.put("gid", fields[1]);
            m_Params.put("lang", fields[2]);
            m_Params.put("name", escape(fields[3]));
            m_Params.put("pref", pref);
            m_Params.put("short", sh);
            lc.executeUpdate("insertName", m_Params);
            count++;
          }
        }

      } while (!done);
    } catch (Exception ex) {
      m_Log.error(m_LogMarker, "importNames::" + m_Params.toString(), ex);
    }
    reader.close();
    m_Log.info(m_LogMarker, m_Messages.get("GeoImport.end", "import", "alternate names", "count", Long.toString(count)));
    m_GIDSDefault.clear();
  }//transformNames

  private String escape(String str) {
    str = str.replaceAll("'", "''");
    str = str.replaceAll("\"", "\\\"");
    return str;
  }//escape

  private BufferedReader getReader(File f) throws IOException {
    return new BufferedReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
  }//getReader

  void runImport(LibraryConnection lc) {
    try {
     long start = System.currentTimeMillis();
     importContinents(lc);
     importTimeZones(lc);
     importCities(lc);
     importCountries(lc);
     importRegions(lc);
     importNames(lc);
     m_Log.debug("runImport()::Import took " + (System.currentTimeMillis()-start) + " ms.");
    } catch (Exception ex) {
      m_Log.error(m_LogMarker, m_Messages.get("GeoImport.error"), ex);
    }
  }//importDatabase

}//classGeoImport
