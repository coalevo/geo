/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.geo.impl;

import com.arimdi.geo.service.GeoService;
import com.arimdi.geo.service.GeoServiceConfiguration;
import net.coalevo.foundation.model.Maintainable;
import net.coalevo.foundation.model.Messages;
import net.coalevo.foundation.util.BundleConfiguration;
import net.coalevo.foundation.util.DummyMessages;
import net.coalevo.logging.model.LogProxy;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Provides a <tt>BundleActivator</tt> implementation for
 * the discussion bundle.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Activator
    implements BundleActivator {

  private static Marker c_LogMarker;
  private static LogProxy c_Log;
  private static Messages c_BundleMessages = new DummyMessages();
  private static ServiceMediator c_Services;
  private static BundleConfiguration c_BundleConfiguration;
  private Thread m_StartThread;

  private static GeoStore c_GeoStore;
  private static GeoServiceImpl c_GeoService;

  public void start(final BundleContext bundleContext)
      throws Exception {
    if (m_StartThread != null && m_StartThread.isAlive()) {
      throw new Exception();
    }
    m_StartThread = new Thread(
        new Runnable() {
          public void run() {
            try {
              //1. Log
              c_LogMarker = MarkerFactory.getMarker(Activator.class.getName());
              c_Log = new LogProxy();
              c_Log.activate(bundleContext);

              //2. Services
              c_Services = new ServiceMediator();
              c_Services.activate(bundleContext);

              //3 Bundle Messages
              c_BundleMessages =
                  c_Services.getMessageResourceService(ServiceMediator.WAIT_UNLIMITED)
                      .getBundleMessages(bundleContext.getBundle());


              //4. Bundle Configuration
              c_BundleConfiguration = new BundleConfiguration(GeoServiceConfiguration.class.getName());
              c_BundleConfiguration.activate(bundleContext);
              c_Services.setConfigMediator(c_BundleConfiguration.getConfigurationMediator());

              //5. Selection Store
              c_GeoStore = new GeoStore();
              c_GeoStore.activate(bundleContext);
              log().debug(c_LogMarker, c_BundleMessages.get("Activator.activation.store"));

              //6. SelectionManagerService
              c_GeoService = new GeoServiceImpl(c_GeoStore);
              if (!c_GeoService.activate(bundleContext)) {
                log().error(c_LogMarker, c_BundleMessages.get("Activator.activation.exception", "service", "GeoService"));
              }
              String[] classes = {GeoService.class.getName(), Maintainable.class.getName()};
              bundleContext.registerService(
                  classes,
                  c_GeoService,
                  null);
              log().debug(c_LogMarker, c_BundleMessages.get("Activator.activation.service", "service", "GeoService"));

            } catch (Exception ex) {
              log().error(c_LogMarker, "start(BundleContext)", ex);
            }
          }//run
        }//Runnable
    );//Thread
    m_StartThread.setContextClassLoader(this.getClass().getClassLoader());
    m_StartThread.start();
  }//start

  public void stop(BundleContext bundleContext)
      throws Exception {

    //wait start
    if (m_StartThread != null && m_StartThread.isAlive()) {
      m_StartThread.join();
      m_StartThread = null;
    }
    if(c_GeoService != null) {
      c_GeoService.deactivate();
      c_GeoService = null;
    }
    if(c_GeoStore != null) {
      c_GeoStore.deactivate();
      c_GeoStore = null;
    }   
    if (c_BundleConfiguration != null) {
      c_BundleConfiguration.deactivate();
      c_BundleConfiguration = null;
    }
    if (c_Services != null) {
      c_Services.deactivate();
      c_Services = null;
    }
    if (c_Log != null) {
      c_Log.deactivate();
      c_Log = null;
    }
    c_LogMarker = null;
    c_BundleMessages = null;
  }//stop

  static ServiceMediator getServices() {
    return c_Services;
  }//getServices

  static Messages getBundleMessages() {
    return c_BundleMessages;
  }//getBundleMessages

  /**
   * Return the bundles logger.
   *
   * @return the <tt>Logger</tt>.
   */
  static Logger log() {
    return c_Log;
  }//log

}//class Activator
