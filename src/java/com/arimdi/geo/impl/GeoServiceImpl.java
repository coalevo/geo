/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.geo.impl;

import com.arimdi.geo.model.GeoLocation;
import com.arimdi.geo.model.GeoLocationName;
import com.arimdi.geo.model.GeoServiceException;
import com.arimdi.geo.model.NoSuchLocationException;
import com.arimdi.geo.service.GeoService;
import net.coalevo.foundation.model.*;
import net.coalevo.security.model.NoSuchActionException;
import net.coalevo.security.model.PolicyProxy;
import net.coalevo.security.model.ServiceAgentProxy;
import org.osgi.framework.BundleContext;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.Set;

/**
 * This class provides an implementation of the {@link GeoService}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class GeoServiceImpl
    extends BaseService
    implements GeoService, Maintainable {

  private Marker m_LogMarker;

  private ServiceAgentProxy m_ServiceAgentProxy;
  private PolicyProxy m_PolicyProxy;

  private GeoManager m_GeoManager;

  GeoServiceImpl(GeoStore store) {
    super(GeoService.class.getName(), ACTIONS);
    m_LogMarker = MarkerFactory.getMarker(GeoServiceImpl.class.getName());
    m_GeoManager = store.getGeoManager();
  }//constructor

  public boolean activate(BundleContext bundleContext) {
    //1. Authenticate ourself
    m_ServiceAgentProxy = new ServiceAgentProxy(this, Activator.log());
    m_ServiceAgentProxy.activate(bundleContext);

    //2. Policy
    m_PolicyProxy = new PolicyProxy(
        getIdentifier(),
        "COALEVO-INF/security/GeoService-policy.xml",
        m_ServiceAgentProxy,
        Activator.log()
    );
    m_PolicyProxy.activate(bundleContext);

    return true;
  }//activate

  public boolean deactivate() {
    if (m_PolicyProxy != null) {
      m_PolicyProxy.deactivate();
      m_PolicyProxy = null;
    }
    if (m_ServiceAgentProxy != null) {
      m_ServiceAgentProxy.deactivate();
      m_ServiceAgentProxy = null;
    }
    return true;
  }//deactivate

  public void doMaintenance(Agent agent) throws SecurityException, MaintenanceException {
    //To change body of implemented methods use File | Settings | File Templates.
  }

  public long getIdentifierForName(String name)
      throws GeoServiceException, NoSuchLocationException {

    return m_GeoManager.getIdentifierForName(name);
  }//getIdentifierForName

  public GeoLocationName getName(long gid)
      throws GeoServiceException, NoSuchLocationException {

    return m_GeoManager.getName(gid);
  }//getName

  public GeoLocationName getName(long gid, String lang)
      throws GeoServiceException, NoSuchLocationException {

    return m_GeoManager.getName(gid, lang);
  }//getName

  public Set<GeoLocationName> listNames(long gid)
      throws GeoServiceException, NoSuchLocationException {

    return m_GeoManager.listNames(gid);
  }//listNames

  public Set<GeoLocationName> listContinents(String lang) throws GeoServiceException {
    return m_GeoManager.listContinents(lang);
  }//listContinents

  public Set<GeoLocationName> listContinents() throws GeoServiceException {
    return m_GeoManager.listContinents();
  }//listContinents

  public Set<GeoLocationName> listContinentsByName(String prefix, String lang)
      throws GeoServiceException {
    return m_GeoManager.listContinentsByName(prefix, lang);
  }//listContinentsByName

  public Set<GeoLocationName> listContinentsByName(String prefix)
      throws GeoServiceException {
    return m_GeoManager.listContinentsByName(prefix);
  }//listContinentsByName

  public GeoLocationName getContinentForCountry(long gid, String lang)
      throws GeoServiceException, NoSuchLocationException {

    return m_GeoManager.getContinentForCountry(gid, lang);
  }//getContinentForCountry

  public GeoLocationName getContinentForCountry(long gid) throws GeoServiceException, NoSuchLocationException {
    return m_GeoManager.getContinentForCountry(gid);
  }//getContinentForCountry

  public Set<GeoLocationName> listCountries(String lang) throws GeoServiceException {
    return m_GeoManager.listCountries(lang);
  }//listCountries

  public Set<GeoLocationName> listCountries() throws GeoServiceException {
    return m_GeoManager.listCountries();
  }//listCountries

  public Set<GeoLocationName> listCountriesByName(String prefix, String lang)
      throws GeoServiceException {
    return m_GeoManager.listCountriesByName(prefix, lang);
  }//listCountriesByName

  public Set<GeoLocationName> listCountriesByName(String prefix)
      throws GeoServiceException {
    return m_GeoManager.listCountriesByName(prefix);
  }//listCountriesByName

  public Set<GeoLocationName> listRegionsByName(String prefix, String lang)
      throws GeoServiceException {
    return m_GeoManager.listRegionsByName(prefix, lang);
  }//listRegionsByName

  public Set<GeoLocationName> listRegionsByName(String prefix)
      throws GeoServiceException {
    return m_GeoManager.listRegionsByName(prefix);
  }//listRegionsByName

  public Set<GeoLocationName> listCitiesByName(String prefix, String lang)
      throws GeoServiceException {
    return m_GeoManager.listCitiesByName(prefix, lang);
  }//listCitiesByName

  public Set<GeoLocationName> listCitiesByName(String prefix)
      throws GeoServiceException {
    return m_GeoManager.listCitiesByName(prefix);
  }//listCitiesByName

  public GeoLocationName getCountryForRegion(long gid, String lang)
      throws GeoServiceException, NoSuchLocationException {

    return m_GeoManager.getCountryForRegion(gid, lang);
  }//getCountryForRegion

  public GeoLocationName getCountryForRegion(long gid) throws GeoServiceException, NoSuchLocationException {
    return m_GeoManager.getCountryForRegion(gid);
  }//getCountryForRegion

  public GeoLocationName getRegionForCity(long gid, String lang)
      throws GeoServiceException, NoSuchLocationException {

    return m_GeoManager.getRegionForCity(gid, lang);
  }//getRegionForCity

  public GeoLocationName getRegionForCity(long gid) throws GeoServiceException, NoSuchLocationException {
    return m_GeoManager.getRegionForCity(gid);
  }//getRegionForCity

  public GeoLocation getCityLocation(long gid)
      throws GeoServiceException, NoSuchLocationException {

    return m_GeoManager.getCityLocation(gid);
  }//getCityLocation

  public GeoLocation getRegionLocation(long gid)
      throws GeoServiceException, NoSuchLocationException {

    return m_GeoManager.getRegionLocation(gid);
  }//getRegionLocation

  public void rebuildDatabase(Agent caller)
      throws SecurityException {

    //1. Policy check
    enforcePolicy(caller, REBUILD_DATABASE);

    //2. Run import on execution service
    Activator.getServices().getExecutionService(ServiceMediator.WAIT_UNLIMITED).execute(
        m_ServiceAgentProxy.getAuthenticPeer(), new Runnable() {
          public void run() {
            try {
              m_GeoManager.rebuildDatabase();
            } catch (Exception ex) {
              Activator.log().error("rebuildDatabase()", ex);
            }
          }
        }
    );
  }//rebuildDatabase

  private void enforcePolicy(Agent caller, Action a) throws SecurityException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), a);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, Activator.getBundleMessages().get("error.action", "action", a.getIdentifier(), "caller", caller.getIdentifier()), ex);
      throw new SecurityException(a.getIdentifier(), ex);
    }
  }//enforcePolicy

  private static Action REBUILD_DATABASE = new Action("rebuildDatabase");

  private static Action[] ACTIONS = {
      REBUILD_DATABASE
  };

}//class GeoServiceImpl
